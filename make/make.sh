#!/bin/bash

BIN=import_XML

apt-get update
apt-get dist-upgrade -y
apt-get autoremove -y
apt-get install qt5-default sqlite3 -y

cd ..
echo "Building…"
qmake $BIN.pro -spec linux-g++
make
mkdir bin
cp $BIN bin/
make clean
rm -rf Makefile .qmake.stash
cp bin/$BIN ./
rm -rf bin/

exit 0
