#include "mvc_view.h"

mvc_view::mvc_view()
{
    mainLayout = new QGridLayout;
    vl_1 = new QVBoxLayout;
    hl_1 = new QHBoxLayout;

    view = new QTableView;
    btn_clear = new QPushButton(tr("Очистить таблицу"));
    btn_clear->setSizePolicy(QSizePolicy(QSizePolicy::Maximum, QSizePolicy::Fixed, QSizePolicy::ToolButton));
    btn_import = new QPushButton(tr("Импорт файлов XML"));
    btn_import->setSizePolicy(QSizePolicy(QSizePolicy::Maximum, QSizePolicy::Fixed, QSizePolicy::ToolButton));
    pbar = new QProgressBar;
    pbar->setRange(0, 100);
    pbar->setValue(0);
    pbar->setFixedWidth(300);
    spr = new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Minimum);

    countError = 0;
    countSuccess = 0;

    hl_1->addWidget(btn_clear);
    hl_1->addWidget(btn_import);
    hl_1->addWidget(pbar);
    hl_1->addSpacerItem(spr);

    vl_1->addWidget(view);
    vl_1->addLayout(hl_1);

    mainLayout->addLayout(vl_1, 0, 0);

    setWindowTitle(tr("Import_XML"));
    setLayout(mainLayout);
    showMaximized();

    popUp = new popup();
    filesError = new QVector<QString>;
    filesError->clear();

    connect(btn_import, SIGNAL(clicked()), this, SLOT(slotImportXML()));
    connect(btn_clear, SIGNAL(clicked()), this, SIGNAL(signalClear()));
}


QString mvc_view::getDirectory()
{
    return QFileDialog::getExistingDirectory(this, tr("Choose folder"), "", QFileDialog::ShowDirsOnly | QFileDialog::DontUseNativeDialog);
}


void mvc_view::setBlock(bool flag)
{
    btn_clear->setDisabled(flag);
    btn_import->setDisabled(flag);
}


void mvc_view::setUnblock(bool flag)
{
    setBlock(!flag);
}


void mvc_view::slotImportXML()
{
    // Блокировка элементов графики
    setBlock(true);

    QString str = getDirectory();
    emit signalImportXML(str);
}


void mvc_view::slotFinishStartApp(QSqlQueryModel* model)
{
    view->setModel(model);
    view->horizontalHeader()->setStretchLastSection(true);
    view->show();
    pbar->reset();
}


void mvc_view::slotProgressBar(unsigned int i)
{
    pbar->setValue(static_cast<int>(i));
}


void mvc_view::slotSetSuccess()
{
    popUp->slotSetPopupTextSuccess(++countSuccess);
}


void mvc_view::slotSetError()
{
    popUp->slotSetPopupTextError(++countError);
}


void mvc_view::slotSetErrorFile(QString str)
{
    filesError->append(str);
    popUp->slotSetPopupTextFiles(*filesError);
}


void mvc_view::slotFinishImportXML(QSqlQueryModel* model)
{
    // Заносим данные в таблицу
    view->setModel(model);
    view->horizontalHeader()->setStretchLastSection(true);
    view->show();

    // Сбрасываем прогрессбар
    pbar->reset();

    // Разблокировка элементов графики
    setUnblock(true);

    // Чистим данные для окна состояния импорта
    countSuccess = 0;
    countError = 0;
    filesError->clear();
}


void mvc_view::slotClear(QSqlQueryModel* model)
{
    view->setModel(model);
    view->horizontalHeader()->setStretchLastSection(true);
    view->show();
}


void mvc_view::closeEvent(QCloseEvent* event)
{
    QMessageBox* exit = new QMessageBox(QMessageBox::Question,
                                        tr("Завершение работы"),
                                        tr("Вы действительно хотите выйти?"),
                                        QMessageBox::Yes | QMessageBox::No);

    int n = exit->exec();
    delete exit;

    if(n == QMessageBox::Yes)
    {
        // --- Нужно добавить сигнал с остановкой потоков, если они есть ---
        event->accept();
    }
    else
    {
        event->ignore();
    }
}
