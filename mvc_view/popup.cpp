#include "popup.h"

popup::popup()
{
    setWindowFlags(Qt::FramelessWindowHint |                                                    // Отключаем оформление окна
                   Qt::Tool |                                                                   // Отменяем показ в качестве отдельного окна
                   Qt::WindowStaysOnTopHint);                                                   // Устанавливаем поверх всех окон
    setAttribute(Qt::WA_TranslucentBackground);                                                 // Указываем, что фон будет прозрачным
    setAttribute(Qt::WA_ShowWithoutActivating);                                                 // При показе, виджет не получает фокуса автоматически

    // Настройка текста уведомления
    label_1 = new QLabel;
    label_2 = new QLabel;
    label_3 = new QLabel;

    label_1->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);                                 // Устанавливаем по центру
    label_2->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);                                 // Устанавливаем по центру
    label_3->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);                                 // Устанавливаем по центру

    // И настраиваем стили
    QString lbl_style = "QLabel {color: #fff;"
                                "margin-top: 6px;"
                                "margin-bottom: 6px;"
                                "margin-left: 10px;"
                                "margin-right: 10px;}";

    label_1->setStyleSheet(lbl_style);
    label_2->setStyleSheet(lbl_style);
    label_3->setStyleSheet(lbl_style);

    clearLabels();

    btn_close = new QPushButton(tr("x"));
    btn_close->setStyleSheet("QPushButton {color: #fff;"
                                          "background-color: rgba(0, 0, 0, 0);"
                                          "border: 0}");
    btn_close->setFixedSize(20, 20);

    spr = new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Minimum);

    hl_1 = new QHBoxLayout;
    hl_1->addSpacerItem(spr);
    hl_1->addWidget(btn_close);

    layout = new QGridLayout;
    layout->addLayout(hl_1, 0, 0);
    layout->addWidget(label_1, 1, 0);
    layout->addWidget(label_2, 2, 0);
    layout->addWidget(label_3, 3, 0);
    setLayout(layout);

    connect(btn_close, SIGNAL(clicked()), this, SLOT(slotHide()));
}


void popup::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event)

    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);          // Включаем сглаживание

    // Подготавливаем фон. rect() возвращает внутреннюю геометрию виджета уведомления, по содержимому
    QRect roundedRect;
    roundedRect.setX(rect().x() + 5);
    roundedRect.setY(rect().y() + 5);
    roundedRect.setWidth(rect().width() - 10);
    roundedRect.setHeight(rect().height() - 10);

    // Кисть настраиваем на чёрный цвет в режиме полупрозрачности 180 из 255
    painter.setBrush(QBrush(QColor(0,0,0,155)));
    painter.setPen(Qt::NoPen); // Край уведомления не будет выделен

    // Отрисовываем фон с закруглением краёв в 10px
    painter.drawRoundedRect(roundedRect, 10, 10);
}


void popup::clearLabels()
{
    label_1->setText("Файлы с ошибками:\n");
    label_2->setText("Успешно: 0");
    label_3->setText("Ошибки: 0");
}


void popup::slotSetPopupTextFiles(const QVector<QString> &text)
{
    label_1->setText("Файлы с ошибками:\n");

    for(int i=0; i<text.size(); i++)
    {
        label_1->setText(label_1->text()+text.at(i)+"\n");  // Устанавливаем текст в label_1
    }

    adjustSize();                                           // Пересчёт размеров уведомления
    this->showWin();
}


void popup::slotSetPopupTextSuccess(const unsigned int &i)
{
    label_2->setText("Успешно: "+QString::number(i));       // Устанавливаем текст в label_2
    adjustSize();                                           // Пересчёт размеров уведомления
    this->showWin();
}


void popup::slotSetPopupTextError(const unsigned int &i)
{
    label_3->setText("Ошибки: "+QString::number(i));        // Устанавливаем текст в label_3
    adjustSize();                                           // Пересчёт размеров уведомления
    this->showWin();
}


void popup::showWin()
{
    setGeometry(QApplication::desktop()->availableGeometry().width() - 36 - width() + QApplication::desktop()->availableGeometry().x(),
                QApplication::desktop()->availableGeometry().height() - 36 - height() + QApplication::desktop()->availableGeometry().y(),
                width(),
                height());

    this->show();                                           // Отображаем виджет
}


void popup::slotHide()
{
    this->hide();                                           // Скрываем виджет
    this->clearLabels();                                    // Чистим содержимое окна
}
