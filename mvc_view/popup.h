#ifndef POPUP_H
#define POPUP_H

#include <QLabel>
#include <QGridLayout>
#include <QPushButton>
#include <QPainter>
#include <QApplication>
#include <QDesktopWidget>


class popup : public QWidget
{
    Q_OBJECT
public:
    explicit popup();
    void clearLabels();

protected:
    void paintEvent(QPaintEvent *event);                    // Фон будет отрисовываться через метод перерисовки

public slots:
    void slotSetPopupTextFiles(const QVector<QString> &);   // Слот устанавливающий текста в уведомление
    void slotSetPopupTextSuccess(const unsigned int &);     // Слот устанавливающий кол-во успешно импортированных записей в уведомление
    void slotSetPopupTextError(const unsigned int &);       // Слот устанавливающий кол-во записей с ошибкой в уведомление

private slots:
    void slotHide();                                        // Слот для проверки виден ли виджет, или его необходимо скрыть

private:
    void showWin();                                         // Метод показа виджета

    QLabel* label_1;                                        // Label_1 со списком файлов
    QLabel* label_2;                                        // Label_2 с кол-вом успешно добавленных строк
    QLabel* label_3;                                        // Label_3 с кол-вом ошибок при добавление строк
    QPushButton* btn_close;                                 // Кнопка закрытия всплывающего окна
    QSpacerItem* spr;                                       // "Пружинка"
    QHBoxLayout* hl_1;                                      // Горизонтальный Layout
    QGridLayout* layout;                                    // Основной Layout
};

#endif // POPUP_H
