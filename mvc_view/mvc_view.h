#ifndef MVC_VIEW_H
#define MVC_VIEW_H

#include <QWidget>
#include <QtGui>
#include <QProgressBar>
#include <QTableView>
#include <QPushButton>
#include <QBoxLayout>
#include <QFileDialog>
#include <QtSql>
#include <QHeaderView>
#include <QMessageBox>

#include "popup.h"


class mvc_view : public QWidget
{
    Q_OBJECT
public:
    explicit mvc_view();
    void closeEvent(QCloseEvent* event);

private:
    QString getDirectory();                         // Получение директории с файлами XML
    void setBlock(bool);                            // Блокировка и разблокировка элементов навигации
    void setUnblock(bool flag);                     // Блокировка и разблокировка элементов навигации

    QGridLayout *mainLayout;
    QVBoxLayout *vl_1;
    QHBoxLayout *hl_1;
    QSpacerItem* spr;

    QTableView *view;
    QPushButton *btn_clear;
    QPushButton *btn_import;
    QProgressBar *pbar;

    popup *popUp;                                   // Всплывающее сообщение с состоянием импорта
    QVector<QString> *filesError;                   // Список фалов с ошибками при импортировании
    unsigned int countError;                        // Кол-во ошибок в файле
    unsigned int countSuccess;                      // Кол-во успешных записей в файле

signals:
    void signalImportXML(QString);                  // Сигнал импортирования XML
    void signalStartApp();                          // Сигнал после запуска приложения
    void signalClear();                             // Сигнал очистки таблицы

public slots:
    void slotFinishStartApp(QSqlQueryModel*);       // Слот окончания взятия данных из БД при запуске приложения
    void slotClear(QSqlQueryModel*);                // Слот очистки таблицы
    void slotProgressBar(unsigned int);             // Слот прогрессбара
    void slotFinishImportXML(QSqlQueryModel*);      // Слот окончания импорта
    void slotSetErrorFile(QString);                 // Слот задающий состояние выполнения
    void slotSetSuccess();                          // Слот обновляющий кол-во успешных записей
    void slotSetError();                            // Слот обновляющий кол-во ошибочных записей

private slots:
    void slotImportXML();                           // Слот импортирования XML
};

#endif // MVC_VIEW_H
