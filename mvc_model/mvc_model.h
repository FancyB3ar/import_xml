#ifndef MVC_MODEL_H
#define MVC_MODEL_H

#include <QObject>
#include <QtXml>
#include <QtSql>
#include <QSqlQuery>

#include "database.h"


class mvc_model : public QObject
{
    Q_OBJECT
public:
    explicit mvc_model();
    ~mvc_model() {}
    void setDir(const QString);                     // Задаём адрес директории из которой считываем файлы

private:
    void listFilesInDir(QDir &);                    // Создание списка файлов в переданной директории
    void parserXML(QString, database*);             // Парсер файлов

    QString strDir;                                 // Название директории из которой считываем файлы
    QVector<QString>* linesFiles;                   // Список фалов в директории
    QSqlQueryModel* model;                          // Модель с данными из БД
    bool addError;                                  // Ошибка при добавлении записей файла
    unsigned int numConnect;                        // Номер подключения к БД
    QString nameDB;                                 // Название БД

signals:
    void signalFinishStartApp(QSqlQueryModel*);     // Сигнал окончания взятия данных из БД при запуске приложения
    void signalProgressBar(unsigned int);           // Сигнал прогрессбара
    void signalFinishImportXML(QSqlQueryModel*);    // Сигнал окончания импортирования XML
    void signalClear(QSqlQueryModel*);              // Сигнал очистки БД
    void signalSetErrorFile(QString);               // Сигнал отправляющий название файла с ошибкой
    void signalSetSuccess();                        // Сигнал обновляющий кол-во успешных записей
    void signalSetError();                          // Сигнал обновляющий кол-во ошибочных записей
    void signalErrorConnectToDB();                  // Сигнал об ошибки при подключении к БД

public slots:
    void slotStartApp();                            // Слот используется для отдачи данны х при запуске приложения
    void slotImportXML();                           // Слот импорта XML
    void slotClear();                               // Слот очистки БД
};

#endif // MVC_MODEL_H
