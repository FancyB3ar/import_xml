#ifndef DATABASE_H
#define DATABASE_H

#include <QObject>
#include <QtSql>
#include <QSqlQuery>

class database : public QObject
{
    Q_OBJECT
public:
    explicit database(QString name, unsigned int count);
    ~database()
    {
        closeDB();
        QSqlDatabase::removeDatabase(nameConnect);
    }

    bool createDB();                                // Создание таблиц
    bool insertDB(QString, QString, QString);       // Вставка данных в таблицу
    bool deleteDB();                                // Удаление данных из таблицы
    QSqlQuery* selectDB();                          // Берём данные из БД и заносим их в полученную модель
    bool isOpen();                                  // Проверка на установление связи и открытие БД
    int countRow();                                 // Количество записей (нужно было для тестирования)

private:
    bool createConnection();                        // Подключение к БД
    void closeDB();                                 // Закрытие БД

    QString pathDB;                                 // Полный адрес с именем БД
    unsigned int numConnect;                        // Номер подключения, для работы с БД из потоков
    QSqlDatabase db;                                // Переменная для работы с БД
    QString nameConnect;                            // Имя подключения
    QSqlQuery *query;                               // Переменная для работы с запросами

signals:
    void signalErrorConnectToDB();

public slots:
};

#endif // DATABASE_H
