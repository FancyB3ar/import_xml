// !!!  вместо numConnect++; создать функцию контролирующую свободные подключения, чтобы небыло переполнения
// !!! Нужно проверять переданную строку в setDir(const QString str)

#include "mvc_model.h"

mvc_model::mvc_model()
{
    numConnect = 0;
    linesFiles = new QVector<QString>;
    model = new QSqlQueryModel;
    addError = false;
    nameDB = "import_XML.db";
}


void mvc_model::slotStartApp()
{
    numConnect++;
    database* db = new database(nameDB, numConnect);
    connect(db, SIGNAL(signalErrorConnectToDB()), this, SIGNAL(signalErrorConnectToDB()));

    // Запрашиваем данные из БД
    model->setQuery(*db->selectDB());

    // Отсылаем модель
    emit signalFinishStartApp(model);

    delete db;
}


void mvc_model::slotImportXML()
{
    numConnect++;
    database* db = new database(nameDB, numConnect);
    connect(db, SIGNAL(signalErrorConnectToDB()), this, SIGNAL(signalErrorConnectToDB()));

    // Проверяем на наличие директории
    if(strDir.isEmpty())
    {
        // Отсылаем предыдущую модель
        emit signalFinishImportXML(model);
    }
    else
    {
        QDir dir = strDir;

        // Заполняем список файлов в полученной директории
        listFilesInDir(dir);

        // Если есть файлы, считываем их
        if(!linesFiles->isEmpty())
        {
            for(int i=0; i<linesFiles->size(); i++)
            {
                // Парсим файлы и собираем данные
                parserXML(linesFiles->at(i), db);

                // Если ошибка была найдена, отправляем название файла где её обнаружили
                if(addError)
                    emit signalSetErrorFile(linesFiles->at(i));

                emit signalProgressBar(static_cast<unsigned int>(100/linesFiles->size()*(i+1)));
            }

            // Берём данные из БД и заносим в модель
            model->setQuery(*db->selectDB());
            emit signalFinishImportXML(model);
        }
        else
        {
            // Отправляем пустое значение для отображения информации для пустых директорий
            emit signalSetErrorFile("");
        }
    }

    delete db;
}


void mvc_model::slotClear()
{
    numConnect++;
    database* db = new database(nameDB, numConnect);
    connect(db, SIGNAL(signalErrorConnectToDB()), this, SIGNAL(signalErrorConnectToDB()));

    db->deleteDB();
    model->setQuery(*db->selectDB());
    emit signalClear(model);

    delete db;
}


void mvc_model::setDir(const QString str)
{
    strDir = str;
}


void mvc_model::parserXML(QString str, database* db)
{
    // Обнуляем состояние наличия ошибки
    addError = false;

    QFile file(str);

    if(file.open(QIODevice::ReadOnly))
    {
        QXmlStreamReader sr(&file);

        do
        {
            sr.readNext();

            // Пишем данные в БД и информируем об успешном добавлении
            if( (sr.tokenString() != "StartDocument") && (sr.tokenString() != "Invalid") && (sr.tokenString() != "EndDocument") )
            {
                if(!db->isOpen())
                {
                    emit signalErrorConnectToDB();
                    return;
                }
                else
                {
                    db->insertDB(sr.tokenString(), sr.name().toString(), sr.text().toString());
                    emit signalSetSuccess();
                }
            }
            else
                if(sr.tokenString() == "Invalid")
                    emit signalSetError();
        }
        while(!sr.atEnd());

        if(sr.hasError())
        {
            // Информируем о наличие ошибок в файле
            addError = true;
        }

        file.close();
    }
    else
    {
        // Информируем об ошибке при открытии файла
        addError = true;
        emit signalSetError();
    }
}


void mvc_model::listFilesInDir(QDir &dir)
{
    linesFiles->clear();

    if(dir.exists())
    {
        dir.setFilter(QDir::Files | QDir::Hidden | QDir::NoSymLinks);
        dir.setSorting(QDir::Name);
        QFileInfoList list = dir.entryInfoList();

        for(int i = 0; i < list.size(); i++)
        {
            QFileInfo fileInfo = list.at(i);
            linesFiles->append(fileInfo.fileName());
        }
    }
}
