#include "database.h"

database::database(QString name, unsigned int count): pathDB(QDir::currentPath()+"/"+name), numConnect(count)
{
    nameConnect = tr("connect_%1").arg(numConnect);
    db = QSqlDatabase::addDatabase("QSQLITE", nameConnect);
    db.setDatabaseName(pathDB);
    createConnection();
    query = new QSqlQuery(db);
}


bool database::createConnection()
{
    if(!db.open())
    {
        qDebug() << "CAN'T OPEN DATABASE: " << db.lastError();
        return false;
    }

    return true;
}


bool database::isOpen()
{
    if(!db.isOpen())
    {
        qDebug() << "DATABASE IS CLOSE: " << db.lastError();
        return false;
    }

    return true;
}


bool database::insertDB(QString s1, QString s2, QString s3)
{
    if(!isOpen())
    {
        emit signalErrorConnectToDB();
        return false;
    }
    else
    {
        QString str = "INSERT INTO data_xml (marker, name, text) VALUES('%1', '%2', '%3');";

        if(!query->exec(str.arg(s1).arg(s2).arg(s3)))
        {
            qDebug() << "DATA DIDN'T INSERT!!!";
            return false;
        }

        return true;
    }
}


bool database::deleteDB()
{
    if(!isOpen())
    {
        emit signalErrorConnectToDB();
        return false;
    }
    else
    {
        QString str = "DELETE FROM data_xml;";

        if(!query->exec(str))
        {
            qDebug() << "DATA DIDN'T DELETE!!!";
            return false;
        }

        return true;
    }
}


QSqlQuery* database::selectDB()
{
    query->clear();

    if(!isOpen())
    {
        emit signalErrorConnectToDB();
        return query;
    }
    else
    {
        createDB();

        QString str = "SELECT marker, name, text FROM data_xml;";

        if(!query->exec(str))
        {
            qDebug() << "DATA DIDN'T SELECT!!!";
            return query;
        }
        else
        {
            // Считываем все данные для отправки
            while(query->next()){}

            return query;
        }
    }
}


int database::countRow()
{
    QString str = "SELECT count(*) FROM data_xml;";
    query->exec(str);
    query->next();
    return query->value(0).toInt();
}


bool database::createDB()
{
    QString str_1 = "CREATE TABLE IF NOT EXISTS data_xml "
                    "( "
                        "id INTEGER PRYMARY KEY, "
                        "marker TEXT, "
                        "name TEXT, "
                        "text TEXT"
                    ");";

    if(!query->exec(str_1))
    {
        qDebug() << "TABLE DIDN'T CREATE!!!";
        return false;
    }

    return true;
}


void database::closeDB()
{
    db.close();
}
