#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "mvc_model.h"
#include "mvc_view.h"


class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    MainWindow();

    ~MainWindow()
    {
        delete model;
        delete view;
    }

private:
    QThread *thread_1;  // Поток для импорта
    mvc_model *model;   // Модель по MVC
    mvc_view *view;     // Графическое представление по MVC

signals:
    void signalFinishImportXML(QSqlQueryModel*);

public slots:
    void slotImportXML(const QString);
    void slotFinishImportXML(QSqlQueryModel*);
};

#endif // MAINWINDOW_H
