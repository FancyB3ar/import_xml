#include "mainwindow.h"

MainWindow::MainWindow()
{
    thread_1 = new QThread;

    model = new mvc_model;
    view = new mvc_view;
    view->show();

    // START
    connect(view, SIGNAL(signalStartApp()), model, SLOT(slotStartApp()));
    connect(model, SIGNAL(signalFinishStartApp(QSqlQueryModel*)), view, SLOT(slotFinishStartApp(QSqlQueryModel*)));     // Окончания взятия данных из БД при запуске приложения

    // XML
    connect(view, SIGNAL(signalImportXML(QString)), this, SLOT(slotImportXML(QString)));                                // Событие начала импорта XML
    connect(this, SIGNAL(signalFinishImportXML(QSqlQueryModel*)), view, SLOT(slotFinishImportXML(QSqlQueryModel*)));    // Окончание импорта XML

    // STATUS OF IMPORT
    connect(model, SIGNAL(signalProgressBar(unsigned int)), view, SLOT(slotProgressBar(unsigned int)));                 // Изменение прогрессбара выполнения импорта
    connect(model, SIGNAL(signalSetSuccess()), view, SLOT(slotSetSuccess()));
    connect(model, SIGNAL(signalSetError()), view, SLOT(slotSetError()));
    connect(model, SIGNAL(signalSetErrorFile(QString)), view, SLOT(slotSetErrorFile(QString)));

    // CLEAR
    connect(view, SIGNAL(signalClear()), model, SLOT(slotClear()));
    connect(model, SIGNAL(signalClear(QSqlQueryModel*)), view, SLOT(slotClear(QSqlQueryModel*)));

    // Взятие данных при запуске
    emit view->signalStartApp();
}


void MainWindow::slotImportXML(const QString str)
{
    model->setDir(str);
    model->moveToThread(thread_1);

    connect(thread_1, SIGNAL(started()), model, SLOT(slotImportXML()));                                                 // Подключение запуска расчёта в потоке
    connect(model, SIGNAL(signalFinishImportXML(QSqlQueryModel*)), this, SLOT(slotFinishImportXML(QSqlQueryModel*)));   // Окончание импорта и остановка потока

    thread_1->start();
}


void MainWindow::slotFinishImportXML(QSqlQueryModel* model)
{
    emit signalFinishImportXML(model);

    thread_1->quit();
    thread_1->wait();

    disconnect(thread_1, SIGNAL(started()), this->model, SLOT(slotImportXML()));                                                 // Подключение запуска расчёта в потоке
    disconnect(this->model, SIGNAL(signalFinishImportXML(QSqlQueryModel*)), this, SLOT(slotFinishImportXML(QSqlQueryModel*)));   // Окончание импорта и остановка потока
}
